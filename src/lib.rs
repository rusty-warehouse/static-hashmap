#![no_std]

pub use macros;

/// A statically-allocated hash-map.
pub struct StrMap<V: 'static> {
    array: [&'static [(&'static str, V)]],
}

const fn string_eql(a: &str, b: &str) -> bool {
    let len = a.len();
    if len != b.len() {
        return false;
    }
    let (a, b) = (a.as_ptr(), b.as_ptr());

    let mut i = 0;
    while i < len {
        let (a, b) = unsafe { (a.add(i).read(), b.add(i).read()) };
        if a != b {
            return false;
        }
        i += 1;
    }
    true
}

impl<V> StrMap<V>
{
    const fn hash(s: &str) -> usize {
        macros::hash!(s)
    }

    pub const fn get<'a>(&'a self, key: &str) -> Option<&'a V> {
        let i = Self::hash(key) % self.array.len();
        let arr = unsafe { *self.array.as_ptr().add(i) };
        let mut i = 0;
        while i < arr.len() {
            let element = unsafe { &*arr.as_ptr().add(i) };
            let (k, v) = (element.0, &element.1);
            if string_eql(k, key) {
                return Some(v);
            }
            i += 1;
        }
        None
    }
}

impl<V: Clone> StrMap<V>
{
    pub fn get_cloned(&self, key: &str) -> Option<V> {
        self.get(key).cloned()
    }
}

#[macro_export]
macro_rules! map {
    {type Item = $t:ty; $($k:literal: $v:expr),*$(,)?} => {{
        let slice: &[&'static [(&'static str, $t)]] = {
            $crate::macros::map!($($k => $v),*)
        };
        unsafe { &*(slice as *const [_] as *const $crate::StrMap<$t>) }
    }};
}